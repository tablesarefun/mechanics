#include "common.h"
#include "collision.h"
#include "mass_object.h"
#include "point_mass.h"

bool has_collision(Mass_Object *m1, Mass_Object *m2)
{
	if (m1->type == MASS_OBJECT_TYPE_POINT && m2->type == MASS_OBJECT_TYPE_POINT) {
		return does_point_point_collide(m1, m2);
	}

	if ((m1->type == MASS_OBJECT_TYPE_POINT && m2->type == MASS_OBJECT_TYPE_PLANE) ||
	    (m2->type == MASS_OBJECT_TYPE_POINT && m1->type == MASS_OBJECT_TYPE_PLANE)) {
		if (m1->type == MASS_OBJECT_TYPE_POINT) {
			return does_point_plane_collide(m1, m2);
		} else {
			return does_point_plane_collide(m2, m1);
		}
	}

	if (m1->type == MASS_OBJECT_TYPE_PLANE && m2->type == MASS_OBJECT_TYPE_PLANE) {
		return does_plane_plane_collide(m1, m2);
	}
}

bool does_point_point_collide(Mass_Object *p1, Mass_Object *p2)
{
	NOT_IMPLEMENTED();
}

bool does_point_plane_collide(Mass_Object *point, Mass_Object *plane)
{
	NOT_IMPLEMENTED();
}

bool does_plane_plane_collide(Mass_Object *p1, Mass_Object *p2)
{
	NOT_IMPLEMENTED();
}

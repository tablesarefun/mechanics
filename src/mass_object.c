#include <stdbool.h>
#include "point_mass.h"
#include "plane_mass.h"
#include "mass_object.h"
#include "common.h"

Mass_Object make_mass_object(enum mass_object_type type,
                             bool is_fixed,
                             Vector2 pos, Vector2 vel, Vector2 acc,
                             float mass)
{
	Mass_Object m = { .type = type, .is_fixed = is_fixed,
	                  .pos = pos, .vel = vel, .acc = acc,
	                  .mass = mass };

	switch (type)
	{
	case MASS_OBJECT_TYPE_POINT:
		// Point mass only has position, velocity, and acceleration
		// It is dimentionless.
		break;
	case MASS_OBJECT_TYPE_PLANE:
		m.as_plane = make_plane_mass(pos, 0);
		break;
	default:
		//TODO: Have some kind of error
	}

	return m;
}

Mass_Object make_point_mass_object(bool is_fixed, Vector2 pos, Vector2 vel,
                                   Vector2 acc, float mass)
{
	return make_mass_object(MASS_OBJECT_TYPE_POINT, is_fixed,
	                        pos, vel, acc, mass);
}

Mass_Object make_plane_mass_object(bool is_fixed, Vector2 pos, Vector2 vel,
                                   Vector2 acc, float mass, float length)
{
	Mass_Object m = make_mass_object(MASS_OBJECT_TYPE_PLANE,
	                                 is_fixed,
	                                 pos, vel, acc,
	                                 mass);

	m.as_plane.length = length;
	m.as_plane.end_pos = (Vector2){ m.pos.x, m.pos.y + length };

	return m;
}

void update_mass_object(Mass_Object *m)
{
	if (m->is_fixed)
		return;

	m->pos = add_vec2(m->pos, m->vel);
	m->vel = add_vec2(m->vel, m->acc);
}

void update_mass_object_collision(Mass_Object *m1, Mass_Object *m2)
{
	NOT_IMPLEMENTED();
}

// TODO: Have some colour attribute in Mass_Object.
void draw_mass_object(Mass_Object *m)
{
	switch (m->type)
	{
	case MASS_OBJECT_TYPE_POINT:
		draw_point_mass(m->pos);
		break;
	case MASS_OBJECT_TYPE_PLANE:
		draw_plane_mass(m->pos, &m->as_plane);
		break;
	}
}

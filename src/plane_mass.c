#include <raylib.h>
#include "linear_algebra.h"
#include "plane_mass.h"

Plane_Mass make_plane_mass(Vector2 pos, float length)
{
	return (Plane_Mass) { .length = length,
	                      .end_pos = (Vector2) { pos.x, pos.y + length } };
}

void draw_plane_mass(Vector2 pos, Plane_Mass *plane)
{
	// TODO: The plane can only be horizontal, however we may want planes
	// at different angles to the horizontal. Planes should have an angle
	// which is to the horizontal so you can have vertical planes as well
	// as planes along any angle.
	Vector2 end_pos = (Vector2){ pos.x + plane->length, pos.y + PLANE_MASS_WIDTH };
	DrawLineV(pos, end_pos, BLUE);
}

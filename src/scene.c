#include <raylib.h>
#include <stdlib.h>
#include "scene.h"
#include "mass_object.h"
#include "linear_algebra.h"
#include "collision.h"
#include "common.h"

Scene make_scene(float x, float y, size_t width, size_t height)
{
	Vector2 pos = make_vec2(x, y);
	// Have errors.
	Mass_Object *objs = malloc(sizeof(*objs));

	return (Scene){ .pos = pos,
	                .width = width, .height = height,
	               	.num_objects = 0,
	                .size = 1,
	               	.objects = objs };
}

void add_mass_object(Scene *s, Mass_Object m)
{
	if (s->num_objects >= s->size) {
		resize_scene(s, s->size * 2);
	}

	s->objects[s->num_objects] = m;
	++s->num_objects;
}

void resize_scene(Scene *s, size_t new_size)
{
	if (new_size <= s->size)
		return;

	// Have errors.
	s->objects = reallocarray(s->objects, new_size, sizeof(*s->objects));
	s->size = new_size;
}

void update_scene(Scene *s)
{
	for (size_t i = 0; i < s->num_objects; ++i) {
		for (size_t j = i+1; j < s->num_objects; ++j) {
			if (has_collision(&s->objects[i], &s->objects[j])) {
				update_mass_object_collision(&s->objects[i], &s->objects[j]);
			}
		}
		update_mass_object(&s->objects[i]);
	}
}

void draw_scene(Scene *s)
{
	DrawRectangle(s->pos.x, s->pos.y, s->width, s->height, GRAY);

	for (size_t i = 0; i < s->num_objects; ++i) {
		draw_mass_object(&s->objects[i]);
	}
}

void destroy_scene(Scene *s)
{
	free(s->objects);
}

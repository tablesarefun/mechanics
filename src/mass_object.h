#ifndef MASS_OBJECT_H_
#define MASS_OBJECT_H_

#include <stdbool.h>
#include <raylib.h>

#include "plane_mass.h"

enum mass_object_type {
	MASS_OBJECT_TYPE_POINT,
	MASS_OBJECT_TYPE_PLANE,
};

// TODO: Have a colour attribute.
// TODO: Have a mass.
typedef struct {
	enum mass_object_type type;
	bool is_fixed;
	Vector2 pos;
	Vector2 vel;
	Vector2 acc;
	float mass;
	union {
		Plane_Mass as_plane;
	};
} Mass_Object;

Mass_Object make_mass_object(enum mass_object_type type,
                             bool is_fixed,
                             Vector2 pos, Vector2 vel, Vector2 acc,
                             float mass);
Mass_Object make_point_mass_object(bool is_fixed, Vector2 pos, Vector2 vel,
                                   Vector2 acc, float mass);
Mass_Object make_plane_mass_object(bool is_fixed, Vector2 pos, Vector2 vel,
                                   Vector2 acc, float mass, float length);
void update_mass_object(Mass_Object *m);
void update_mass_object_collision(Mass_Object *m1, Mass_Object *m2);
void draw_mass_object(Mass_Object *m);

#endif

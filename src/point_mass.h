#ifndef POINT_MASS_H_
#define POINT_MASS_H_

#include "linear_algebra.h"

#define POINT_MASS_RADIUS 2

void draw_point_mass(Vector2 pos);

#endif

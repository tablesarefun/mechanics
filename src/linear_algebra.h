#ifndef LINEAR_ALGEBRA_H_
#define LINEAR_ALGEBRA_H_

#include <raylib.h>

Vector2 make_vec2(float x, float y);
Vector2 make_zero_vec2(void);
Vector2 scale_vec2(Vector2 v, float s);
Vector2 add_vec2(Vector2 v, Vector2 u);
Vector2 sub_vec2(Vector2 v, Vector2 u);
float vec2_magnitude_squared(Vector2 v);
float vec2_magnitude(Vector2 v);
float dot_vec2(Vector2 v, Vector2 u);

#endif

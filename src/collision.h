#ifndef COLLISION_H_
#define COLLISION_H_

#include <stdbool.h>
#include "mass_object.h"

bool has_collision(Mass_Object *m1, Mass_Object *m2);
bool does_point_point_collide(Mass_Object *p1, Mass_Object *p2);
bool does_point_plane_collide(Mass_Object *point, Mass_Object *plane);
bool does_plane_plane_collide(Mass_Object *p1, Mass_Object *p2);

#endif

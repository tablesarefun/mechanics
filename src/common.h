#ifndef COMMON_H_
#define COMMON_H_

#include <stdio.h>
#include <stdlib.h>

#define NOT_IMPLEMENTED() do {                             \
fprintf(stderr, "%s() not implemented from %s, line %d\n", \
		__FUNCTION__, __FILE__, __LINE__);                 \
exit(0);                                                   \
} while (0);

#endif

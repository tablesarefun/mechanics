#include <raylib.h>

#include "scene.h"
#include "mass_object.h"
#include "linear_algebra.h"
#include "constants.h"

#define WIDTH 800
#define HEIGHT 600

int main(void)
{
	InitWindow(WIDTH, HEIGHT, "mechanics");
	SetTargetFPS(30);

	Scene scene = make_scene(0, 0, WIDTH, HEIGHT);

	add_mass_object(&scene, make_point_mass_object(false,
	                                               make_vec2(WIDTH/2, 0.0f),
	                                               make_zero_vec2(),
	                                               make_vec2(0.0f, 0.1*g),
	                                               5));

	float plane_length = 300.0f;
	add_mass_object(&scene, make_plane_mass_object(true,
	                                               make_vec2((WIDTH - plane_length)/2,
	                                                          HEIGHT - 50),
	                                               make_zero_vec2(),
	                                               make_zero_vec2(),
	                                               10,
	                                               plane_length));

	while (!WindowShouldClose()) {
		update_scene(&scene);

		BeginDrawing();
			ClearBackground(RAYWHITE);
			draw_scene(&scene);
		EndDrawing();
	}

	CloseWindow();
	destroy_scene(&scene);

	return 0;
}

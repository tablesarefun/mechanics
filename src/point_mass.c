#include <raylib.h>
#include "linear_algebra.h"
#include "point_mass.h"

void draw_point_mass(Vector2 pos)
{
	DrawCircleV(pos, POINT_MASS_RADIUS, RED);
}

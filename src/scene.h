#ifndef SCENE_H_
#define SCENE_H_

#include <stddef.h>
#include "mass_object.h"
#include "linear_algebra.h"

// Have a background colour.
typedef struct {
	Vector2 pos;
	size_t width;
	size_t height;
	size_t num_objects;
	size_t size;
	Mass_Object *objects;
} Scene;

Scene make_scene(float x, float y, size_t width, size_t height);
void add_mass_object(Scene *s, Mass_Object m);
void resize_scene(Scene *s, size_t new_size);
void update_scene(Scene *s);
void draw_scene(Scene *s);
void destroy_scene(Scene *s);

#endif

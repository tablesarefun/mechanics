exec = mech
sources = $(wildcard src/*.c)
objects = $(sources:.c=.o)
flags = -lraylib -lm -Wall -Wextra -Wpedantic -Werror

$(exec): $(objects)
	gcc $(objects) $(flags) -o $(exec)
	rm -rf $(objects)

%.o: %.c include/%.h
	gcc -c $(flags) $< -o $@

clean:
	rm $(exec)

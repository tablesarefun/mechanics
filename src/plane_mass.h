#ifndef PLANE_MASS_H_
#define PLANE_MASS_H_

#include "linear_algebra.h"

#define PLANE_MASS_WIDTH 1

typedef struct {
	float length;
	Vector2 end_pos;
} Plane_Mass;

Plane_Mass make_plane_mass(Vector2 pos, float length);
void draw_plane_mass(Vector2 pos, Plane_Mass *plane);

#endif


#include <math.h>
#include "linear_algebra.h"

Vector2 make_vec2(float x, float y)
{
	return (Vector2){ .x = x, .y = y };
}

Vector2 make_zero_vec2(void)
{
	return (Vector2) { .x = 0, .y = 0 };
}

Vector2 scale_vec2(Vector2 v, float s)
{
	return (Vector2) { .x = s*v.x, .y = s*v.y };
}

Vector2 add_vec2(Vector2 v, Vector2 u)
{

	return (Vector2) { .x = v.x + u.x,
	                 .y = v.y + u.y };
}

Vector2 sub_vec2(Vector2 v, Vector2 u)
{
	return (Vector2) { .x = v.x - u.x,
	                 .y = v.y - u.y };
}

float vec2_magnitude_squared(Vector2 v)
{
	return v.x*v.x + v.y*v.y;
}

float vec2_magnitude(Vector2 v)
{
	return sqrt(v.x*v.x + v.y*v.y);
}

float dot_vec2(Vector2 v, Vector2 u)
{
	return v.x*u.x + v.y*u.y;
}

